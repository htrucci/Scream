package com.htrucci.scream;


import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.ValueCallback;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings.PluginState;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.parse.Parse;
import com.parse.ParseACL;
import com.parse.ParseAnalytics;
import com.parse.ParseInstallation;
import com.parse.ParsePush;
import com.parse.ParseUser;
import com.parse.PushService;

public class MainActivity extends Activity {

	
	private List<String> registrationIds = new ArrayList<String>();
	private static final String TAG = "GCM";
	private static String regId;
	private static String COLLAPSE_KEY = String.valueOf(Math.random() % 100 + 1);
	private static boolean DELAY_WHILE_IDLE = true;
	private static int TIME_TO_LIVE = 3;
	private static String JSON = null;
	private static  final String SELECT_PAGE = "http://scream94.net/select_registration.php";
	private static int RETRY = 3;
	final Context context = this;
	AsyncTask<Void, Void, Void> mSendTask;
	private Context appContext = null; //App Context
	
	WebView mWebView;
	private ValueCallback<Uri> mUploadMessage;
	private final static int FILECHOOSER_RESULTCODE = 1;
	
	protected void onActivityResult(int requestCode, int resultCode, Intent intent){
		if(requestCode == FILECHOOSER_RESULTCODE){
			if(null == mUploadMessage)
				return;
			Uri result = intent == null || resultCode != RESULT_OK ? null : intent.getData();
			mUploadMessage.onReceiveValue(result);
			mUploadMessage = null;
		}
	}
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		/*ParseInstallation.getCurrentInstallation().saveInBackground();*/
		setContentView(R.layout.activity_main);
		Parse.initialize(this, "Q4Y7m378RAtDQ3jcPz5SUsXWM2hPjlhQ31kRI51D", "baEhf0qWFs6E4bOjGhIKgEk9Jl0aZ2q9eFArJ6vw");
		PushService.subscribe(context, "Giants", MainActivity.class);
		PushService.setDefaultPushCallback(this, MainActivity.class);
		startActivity(new Intent(this, SplashActivity.class));
		ParseUser.enableAutomaticUser();
		ParseACL defaultACL = new ParseACL();
		// If you would like all objects to be private by default, remove this line.
		defaultACL.setPublicReadAccess(true);
		ParseACL.setDefaultACL(defaultACL, true);
		ParseAnalytics.trackAppOpened(getIntent());
/*		ParseInstallation installation = ParseInstallation.getCurrentInstallation();
		installation.put("user",ParseUser.getCurrentUser());
		installation.saveInBackground();
*//*		initialize();
		startGCM();
		GCMRegistrar.checkDevice(this);
		GCMRegistrar.checkManifest(this);
		regId = GCMRegistrar.getRegistrationId(this);
		if("".equals(regId))
			GCMRegistrar.register(this, "869133430251");
		else
			Log.d("GCM",regId);
			Log.d("GCM","regId is already");
*/		
		mWebView = (WebView)findViewById(R.id.webview);
		mWebView.getSettings().setJavaScriptEnabled(true);
		mWebView.getSettings().setPluginState(PluginState.ON);
		mWebView.getSettings().setBuiltInZoomControls(true);
		mWebView.getSettings().setSupportZoom(true);
		mWebView.setWebChromeClient(new WebChromeClient()
		{
			public void openFileChooser( ValueCallback<Uri> uploadMsg, String acceptType, String capture){
				mUploadMessage = uploadMsg;
				Intent i = new Intent(Intent.ACTION_GET_CONTENT);
				i.addCategory(Intent.CATEGORY_OPENABLE);
				i.setType("*/*");
				MainActivity.this.startActivityForResult(Intent.createChooser(i, "사진을 선택하세요"), FILECHOOSER_RESULTCODE);
			}
		});
		mWebView.loadUrl("http://scream94.net");
		mWebView.setWebViewClient(new mWebViewClient());
	
	}

	public boolean onKeyDown(int keyCode, KeyEvent event){
		if((keyCode == KeyEvent.KEYCODE_BACK) && mWebView.canGoBack()){
			mWebView.goBack();
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}
	
	private class mWebViewClient extends WebViewClient{
		public boolean shouldOverrideUrlLoading(WebView view, String url){
			view.loadUrl(url);
			return true;
		}
	}

	
	private void initialize(){
		appContext = getApplicationContext();
	}
	//GCM부분
	private void startGCM(){
		

	}
	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		
	}

/*	private void sendToDevice(final String msg){
		mSendTask = new AsyncTask<Void, Void, Void>(){

			@Override
			protected Void doInBackground(Void... params) {
				Message.Builder messageBuilder = new Message.Builder();
				messageBuilder.addData("msg", msg);
				messageBuilder.addData("action", "Show");
				Message message = messageBuilder.build();
				try{
					result = sender.send(message, regId, 5);
					Log.d("GCM", "MESSAGE SENT. result : " + result);
				}catch(Exception e){
					e.printStackTrace();
					Log.d("GCM", "MESSAGE FAIL. result : ");
				}
				return null;
			}
			protected void onPostExecute(Void result){
				mSendTask = null;
			}
		};
		mSendTask.execute(null,null,null);
	}*/
	@Override
	public boolean onCreatePanelMenu(int featureId, Menu menu) {
		menu.add(0, 0, 0, "Send");
		return super.onCreatePanelMenu(featureId, menu);
	}
	@Override
	public boolean onMenuItemSelected(int featureId, MenuItem item) {
		if(item.getItemId() == 0){
			Toast.makeText(getApplicationContext(), "SEND", Toast.LENGTH_LONG).show();
			final Dialog senddialog = new Dialog(context); 
			senddialog.setContentView(R.layout.senddialoglayout);
			senddialog.setTitle("스크리머 전체에게 메시지 보내기");
			final EditText editname = (EditText)senddialog.findViewById(R.id.editname);
			Button sendbtn = (Button)senddialog.findViewById(R.id.sendBtn);
			sendbtn.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					Log.e("GCM", "SEND버튼 콜");
					String msg = editname.getText().toString();
					ParsePush push = new ParsePush();
					push.setChannel("Giants");
					push.setMessage(msg);
					push.sendInBackground();
					/*sendToDevice(msg);*/
					senddialog.dismiss();
				}
			});
			senddialog.show();
		}
		return super.onMenuItemSelected(featureId, item);
	}
	
	}