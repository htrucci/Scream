package com.htrucci.scream;

import com.parse.Parse;
import com.parse.PushService;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;

public class SplashActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.splash);
		Parse.initialize(this, "Q4Y7m378RAtDQ3jcPz5SUsXWM2hPjlhQ31kRI51D", "baEhf0qWFs6E4bOjGhIKgEk9Jl0aZ2q9eFArJ6vw");
		PushService.setDefaultPushCallback(this, MainActivity.class);
		Handler handler = new Handler(){
			
			public void handleMessage(Message msg){
				finish();
			}
		};
		handler.sendEmptyMessageDelayed(0, 3000);
	}

	
}
